FROM nvcr.io/nvidia/tensorflow:21.10-tf1-py3
 
MAINTAINER James Brogan &lt;someone@somewhere.com&gt;
 
#  Set initial working directory
WORKDIR /home/bitfusion/downloads/
 
# Update package list
RUN apt-get update
 
# Install Bitfusion. Assumes deb for Ubuntu16.04
# resides in mounted directory, /pkgs
COPY bitfusion-client-ubuntu2004_4.0.1-5_amd64.deb .
RUN apt-get install -y ./bitfusion-client-ubuntu2004_4.0.1-5_amd64.deb
# Must run list_gpus to pull in env and tokens
#RUN bitfusion list_gpus
 
# TF benchmarks
WORKDIR /home/bitfusion/
RUN git clone https://github.com/tensorflow/benchmarks.git
#  Set working directory
WORKDIR /home/bitfusion/benchmarks/
RUN git checkout cnn_tf_v1.13_compatible
 
#  Set working directory
WORKDIR /home/bitfusion/
